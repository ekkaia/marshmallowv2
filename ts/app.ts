import express = require("express");
import bodyParser = require("body-parser");
import http = require("http");
import signale = require("signale");
import io = require("socket.io");
import passport = require("passport");
import passportLocal = require("passport-local");
import mongoose = require("mongoose");
import bcrypt = require("bcryptjs");
import { MongoError } from "mongodb";

// Express configuration
const PORT: number = 3000;
const PUBLIC_DIR: string = "build";
const app: express.Application = express();

const server = new http.Server(app)

app.set("view engine", "ejs");
app.use(express.static(PUBLIC_DIR));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Home page
app.get("/", (req: express.Request, res: express.Response) => {
	res.render("index", { title: "cortana" });
});

// Passport
const localStrategy = passportLocal.Strategy;

app.use(passport.initialize());
app.use(passport.session());

app.get("/success", (req: express.Request, res: express.Response) => {
	res.send(`Welcome ${req.query.username} !`);
});

app.get("/error", (req: express.Request, res: express.Response) => {
	res.send("error logging in");
});

passport.serializeUser((user: any, done: any) => {
	done(null, user.id);
});

passport.deserializeUser((id: any, done: any) => {
	User.findById(id, (err: any, user: any) => {
		done(err, user);
	});
});

// Mongoose
mongoose.Promise = Promise;

mongoose.connect("mongodb://root:admin123@ds034797.mlab.com:34797/marshmallow2", { useNewUrlParser: true }, (err : MongoError) => {
	signale.success("Connexion établie avec la base de données");
});
mongoose.Schema
const userSchema : mongoose.Schema = new mongoose.Schema ({
	username: String,
	password: String
});

const userModel = mongoose.model("userInfo", userSchema, "userInfo");

// Passport local
passport.use(new localStrategy(
  	(username : string, password : string, done) => {
		userModel.findOne({
        	username: username
      	}, (err : express.ErrorRequestHandler, user) => {
        if (err) {
        	return done(err);
        }

        if (!user) {
        	return done(null, false);
        }

        if (user.password != password) {
        	return done(null, false);
        }
		
		return done(null, user);
      });
  	}
));


app.post("/",
  	passport.authenticate("local", { failureRedirect: "/error" }), (req : express.Request, res : express.Response) => {
    	res.redirect(`/success?username=${req.user.username}`);
});

// Start server
server.listen(PORT || process.env.PORT, () => {
	signale.start(`Le serveur est lancé sur le port ${PORT}`);
});